import { Navigate, NavLink, useNavigate } from "react-router-dom";
import { ProtectedRoutes } from "../../Routes/ProtectedRoutes";
import "./Header.css";
import React from "react";
import logoImage from "./../../images/logo.png";
import { useDispatch } from "react-redux";
import { logoutUser } from "../../Redux/Actions/LoginActions";

export const Header = () => {
  let loggedIn = localStorage.getItem("loggedIn");
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onClikLogout = () => {
    localStorage.clear();
    console.log(localStorage);
    navigate("/", { replace: true });
    dispatch(logoutUser());
  };

  return (
    <div className="header-container">
      <header className="menu-container d-none d-md-block">
        <nav className="nav-container">
          <p className="navbar-brand">
            <img src={logoImage} alt="Logo" />
          </p>
          <ul>
            <li className="nav-item">
              <NavLink
                to={"dashboard"}
                className={(status) => (status.isActive ? "active-link" : "")}>
                <span className="material-icons material-symbols-outlined">
                  dashboard
                </span>
                <p className="m-0">Dashboard</p>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to={"/admin/items"}
                className={(status) => (status.isActive ? "active-link" : "")}>
                <span className="material-icons material-symbols-outlined">
                  list_alt
                </span>
                <p className="m-0">Items</p>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to={"billing"}
                className={(status) => (status.isActive ? "active-link" : "")}>
                <span className="material-icons material-symbols-outlined">
                  receipt_long
                </span>
                <p className="m-0">Billing</p>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to={"orders"}
                className={(status) => (status.isActive ? "active-link" : "")}>
                <span className="material-icons material-symbols-outlined">
                  shopping_bag
                </span>
                <p className="m-0">Orders</p>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to={"inventory"}
                className={(status) => (status.isActive ? "active-link" : "")}>
                <span className="material-icons material-symbols-outlined">
                  inventory_2
                </span>
                <p className="m-0">Inventory</p>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink onClick={() => onClikLogout()}>
                <span className="material-icons material-symbols-outlined">
                  logout
                </span>
                <p className="m-0">Logout</p>
              </NavLink>
            </li>
          </ul>
        </nav>
      </header>
      <main className="main-container">
        <ProtectedRoutes />
        {/* {loggedIn ? <ProtectedRoutes /> : <Navigate to={"/"} replace />} */}
      </main>
    </div>
  );
};
