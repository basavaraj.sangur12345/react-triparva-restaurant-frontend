import { applyMiddleware, compose, createStore } from "redux";
import { RootReducers } from "./Reducers";
import createSagaMiddleware from "redux-saga";
import RootSaga from "../Redux-Saga/RootSaga";

const sagaMiddleWare = createSagaMiddleware();
const composeEnhancer = compose;

export const store = createStore(
  RootReducers,
  composeEnhancer(applyMiddleware(sagaMiddleWare))
);

sagaMiddleWare.run(RootSaga);
