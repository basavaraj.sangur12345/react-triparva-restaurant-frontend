import { ActionConstants } from "../Action-Constants/ActionConstants";

// Initial state
let initState = { loading: true, error: "" };

export const DashboardReducer = (state = initState, action) => {
  switch (action.type) {
    // Load dashboard data
    case ActionConstants.DASHBOARD_DATA:
      return { ...state, loading: true, error: "" };
    case ActionConstants.DASHBOARD_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        dashboardData: action.payload,
        error: "",
      };
    case ActionConstants.DASHBOARD_DATA_ERROR:
      return { ...state, loading: false, error: "" };
    // Ends here

    // Load dashboard data ordes
    case ActionConstants.DASHBOARD_ORDERS:
      return { ...state, loading: true, error: "" };
    case ActionConstants.DASHBOARD_ORDERS_SUCCESS:
      return {
        ...state,
        loading: false,
        dashboardOrders: action.payload,
        error: "",
      };
    case ActionConstants.DASHBOARD_ORDERS_ERROR:
      return { ...state, loading: false, error: "" };
    // Ends here

    // Load dashboard data ordes
    case ActionConstants.DASHBOARD_DELETE:
      return { ...state, loading: true, error: "" };
    case ActionConstants.DASHBOARD_DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        dashboardDelete: action.payload,
        error: "",
      };
    case ActionConstants.DASHBOARD_DELETE_ERROR:
      return { ...state, loading: false, error: "" };
    // Ends here

    default:
      return state;
  }
};
