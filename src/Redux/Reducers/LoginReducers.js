import { ActionConstants } from "../Action-Constants/ActionConstants";

let initState = {
  data: {},
  loading: false,
  error: "",
};
export const LoginReducers = (state = initState, action) => {
  switch (action.type) {
    case ActionConstants.LOAD_USERS:
      return { ...state, loading: true, error: "" };
    case ActionConstants.LOAD_USERS_SUCCESS:
      return { data: action.payload, loading: false };
    case ActionConstants.LOAD_USERS_ERROR:
      return { ...state, loading: false, error: action };
    case ActionConstants.LOGOUT_USER:
      return initState;
    default:
      return state;
  }
};
