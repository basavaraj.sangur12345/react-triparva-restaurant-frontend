import { ActionConstants } from "../Action-Constants/ActionConstants";

let initState = {};
export const BillingReducer = (state = initState, action) => {
  switch (action.type) {
    // reducer for loading invoice data
    case ActionConstants.LOAD_INVOICE_DATA:
      return { ...state, loading: true, error: "" };
    case ActionConstants.LOAD_INVOICE_DATA_SUCCESS:
      return { ...state, loading: false, invoice: action.payload, error: "" };
    case ActionConstants.LOAD_INVOICE_DATA_ERROR:
      return { ...state, loading: true, error: action };
    // Ends here

    // reducer for checking table number on selection of table number
    case ActionConstants.CHECK_FOR_TABLE_NUMBER:
      return { ...state, loading: true, error: "" };
    case ActionConstants.CHECK_FOR_TABLE_NUMBER_SUCCESS:
      return { ...state, loading: false, tableData: action.payload, error: "" };
    case ActionConstants.CHECK_FOR_TABLE_NUMBER_ERROR:
      return { ...state, loading: true, error: action };
    // Ends here

    // reducer for checking table number on selection of table number
    case ActionConstants.SUBMIT_NEW_ORDER:
      return { ...state, loading: true, error: "" };
    case ActionConstants.SUBMIT_NEW_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        newOrderData: action.payload,
        error: "",
      };
    case ActionConstants.SUBMIT_NEW_ORDER_ERROR:
      return { ...state, loading: true, error: action };
    // Ends here

    // after adding orders and on page load all the orders
    case ActionConstants.LOAD_ORDERS:
      return { ...state, loading: true, error: "" };
    case ActionConstants.LOAD_ORDERS_SUCCESS:
      return {
        ...state,
        loading: false,
        orders: action.payload,
        error: "",
      };
    case ActionConstants.LOAD_ORDERS_ERROR:
      return { ...state, loading: true, error: action };
    // Ends here

    // after adding orders and on page load all the orders
    case ActionConstants.FIND_ORDER:
      return { ...state, loading: true, error: "" };
    case ActionConstants.FIND_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        findOrder: action.payload,
        error: "",
      };
    case ActionConstants.FIND_ORDER_ERROR:
      return { ...state, loading: true, error: action };

    // Update orders
    case ActionConstants.UPDATE_ORDER:
      return { ...state, loading: true, error: "" };
    case ActionConstants.UPDATE_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        orderUpdate: action.payload,
        error: "",
      };
    case ActionConstants.UPDATE_ORDER_ERROR:
      return { ...state, loading: true, error: action };
    // Ends here

    // Update orders
    case ActionConstants.BILLING_RESET:
      return { ...state, tableData: null };
    // Ends here
    default:
      return state;
  }
};
