import { combineReducers } from "redux";
import { BillingReducer } from "./BillingReducer";
import { DashboardReducer } from "./DashboardReducer";
import { itemsReducer } from "./ItemsReducer";
import { LoginReducers } from "./LoginReducers";

export const RootReducers = combineReducers({
  loginReducers: LoginReducers,
  itemsReducer: itemsReducer,
  billingReducer: BillingReducer,
  dashboardReducer: DashboardReducer,
});
