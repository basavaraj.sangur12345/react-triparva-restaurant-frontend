import { ActionConstants } from "../Action-Constants/ActionConstants";

let initState = {};
export const itemsReducer = (state = initState, action) => {
  switch (action.type) {
    case ActionConstants.LOAD_ITEMSLIST:
      return { ...state, loading: true, error: "" };
    case ActionConstants.LOAD_ITEMSLIST_SUCCESS:
      return { ...state, itemsList: action.payload, loading: false };
    case ActionConstants.LOAD_ITEMSLIST_ERROR:
      return { ...state, loading: false, error: action };
    case ActionConstants.ADD_ITEMLIST:
      return { ...state, added: action.payload, loading: true, error: "" };
    case ActionConstants.ADD_ITEMLIST_SUCCESS:
      return { ...state, loading: false };
    case ActionConstants.ADD_ITEMLIST_ERROR:
      return { ...state, loading: false, error: action };
    case ActionConstants.DELETE_ITEMLIST:
      return { ...state, added: action.payload, loading: true, error: "" };
    case ActionConstants.DELETE_ITEMLIST_SUCCESS:
      return { ...state, loading: false };
    case ActionConstants.DELETE_ITEMLIST_ERROR:
      return { ...state, loading: false, error: action };
    case ActionConstants.EDIT_ITEMLIST:
      return { ...state, edited: action.payload, loading: true, error: "" };
    case ActionConstants.EDIT_ITEMLIST_SUCCESS:
      return { ...state, loading: false };
    case ActionConstants.EDIT_ITEMLIST_ERROR:
      return { ...state, loading: false, error: action };
    case ActionConstants.LOAD_SINGLE_ITEMSLIST:
      return { ...state, loading: true, error: "" };
    case ActionConstants.LOAD_SINGLE_ITEMSLIST_SUCCESS:
      return { ...state, itemDetails: action.payload, loading: false };
    case ActionConstants.LOAD_SINGLE_ITEMSLIST_ERROR:
      return { ...state, loading: false, error: action };
    default:
      return state;
  }
};
