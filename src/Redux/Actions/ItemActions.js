import { ActionConstants } from "../Action-Constants/ActionConstants";

export const loadItemList = () => {
  return {
    type: ActionConstants.LOAD_ITEMSLIST,
  };
};

export const addItems = (inputData) => {
  return {
    type: ActionConstants.ADD_ITEMLIST,
    payload: inputData,
  };
};

export const getsingleitem = (inputData) => {
  return {
    type: ActionConstants.LOAD_SINGLE_ITEMSLIST,
    payload: inputData,
  };
};

export const editItems = (inputData) => {
  return {
    type: ActionConstants.EDIT_ITEMLIST,
    payload: inputData,
  };
};

export const deleteItems = (inputData) => {
  return {
    type: ActionConstants.DELETE_ITEMLIST,
    payload: inputData,
  };
};
