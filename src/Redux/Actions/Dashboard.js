import { ActionConstants } from "../Action-Constants/ActionConstants";

export const getDataDashboard = () => {
  return {
    type: ActionConstants.DASHBOARD_DATA,
  };
};

export const getDashboardOrders = (payload) => {
  return {
    type: ActionConstants.DASHBOARD_ORDERS,
    payload: payload,
  };
};

export const deleteDashboardOrder = (payload) => {
  return {
    type: ActionConstants.DASHBOARD_DELETE,
    payload: payload,
  };
};
