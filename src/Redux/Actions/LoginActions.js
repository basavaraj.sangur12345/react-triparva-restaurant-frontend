import { ActionConstants } from "../Action-Constants/ActionConstants";

export const setLoginData = (userData) => {
  return {
    type: ActionConstants.LOAD_USERS,
    payload: userData,
  };
};

export const logoutUser = () => {
  return {
    type: ActionConstants.LOGOUT_USER,
  };
};
