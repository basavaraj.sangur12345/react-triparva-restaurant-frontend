import { ActionConstants } from "../Action-Constants/ActionConstants";

export const loadInvoiceData = () => {
  return {
    type: ActionConstants.LOAD_INVOICE_DATA,
  };
};

export const onActionSelectTableNumber = (payload) => {
  return {
    type: ActionConstants.CHECK_FOR_TABLE_NUMBER,
    payload: payload,
  };
};

export const onSaveOrder = (payload) => {
  return {
    type: ActionConstants.SUBMIT_NEW_ORDER,
    payload: payload,
  };
};

export const onLoadOrders = () => {
  return {
    type: ActionConstants.LOAD_ORDERS,
  };
};

export const findOrder = (payload) => {
  return {
    type: ActionConstants.FIND_ORDER,
    payload: payload,
  };
};

export const updateOrder = (payload) => {
  return {
    type: ActionConstants.UPDATE_ORDER,
    payload: payload,
  };
};

export const billingResetAll = () => {
  return {
    type: ActionConstants.BILLING_RESET,
  };
};
