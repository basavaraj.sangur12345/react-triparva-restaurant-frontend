import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import React, { useMemo, useState } from "react";
import { InputForm } from "../input";
import "./ManualCashForm.css";

export const ManualCashForm = (props) => {
  // Default states for the manual cash form states
  const defaultManualCashStates = {
    itemName: "",
    quantity: "",
    price: "",
    cashAdded: true,
    singlePrice: "",
  };
  let [manualCashFormStates, setManualCashFormStates] = useState(
    defaultManualCashStates
  );

  // If manual cash editable data is exist fill the form with old values
  useMemo(() => {
    if (props.manualCashEdit) {
      setManualCashFormStates((states) => ({
        ...states,
        ...props.manualCashEdit,
      }));
    }
  }, [props.manualCashEdit]);

  // On change form input values data
  const handleFormInputChange = (e) => {
    setManualCashFormStates({
      ...manualCashFormStates,
      [e.target.name]: e.target.value,
    });
  };

  // Manual cash form input fields
  let manuCashFormInputs = [
    {
      name: "itemName",
      type: "text",
      label: "Item Name",
      placeHolder: "Item Name",
      value: manualCashFormStates["itemName"],
      onChange: handleFormInputChange,
    },
    {
      name: "singlePrice",
      type: "number",
      label: "Price",
      placeHolder: "Price",
      value: manualCashFormStates["singlePrice"],
      onChange: handleFormInputChange,
    },
    {
      name: "quantity",
      type: "number",
      label: "Quantity",
      placeHolder: "Quantity",
      value: manualCashFormStates["quantity"],
      onChange: handleFormInputChange,
    },
  ];

  /**@function handleOnClose - When user clicks on close button or on overlay */
  const handleOnClose = () => {
    props.onClose();
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    manualCashFormStates.price =
      manualCashFormStates.singlePrice * manualCashFormStates.quantity;
    props.onClose(manualCashFormStates);
    setManualCashFormStates(defaultManualCashStates); // Reset the form values once submit
  };

  return (
    <Dialog
      className="manualCashDialog"
      open={props.open}
      onClose={() => handleOnClose()}
    >
      <DialogTitle>Manual Cash</DialogTitle>
      <form onSubmit={handleOnSubmit} autoComplete="off">
        <DialogContent>
          {manuCashFormInputs.map((element, index) => {
            return (
              <div key={index}>
                <InputForm inputValues={element} />
              </div>
            );
          })}
        </DialogContent>
        <DialogActions>
          <button
            type="reset"
            className="btn btn-secondary"
            onClick={() => handleOnClose()}
          >
            Cancel
          </button>
          <button className="btn btn-primary" type="submit">
            Submit
          </button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
