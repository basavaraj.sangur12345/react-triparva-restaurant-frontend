import loading from "./../../assets/loader.gif";
import "./Loading.css";
import React from "react";

export const Loading = () => {
  return (
    <div className="loading">
      <img src={loading} alt="" />
    </div>
  );
};
