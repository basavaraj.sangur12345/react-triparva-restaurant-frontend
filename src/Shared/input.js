import { MenuItem, TextField } from "@mui/material";
import React, { useState } from "react";

export const InputForm = ({ inputValues }) => {
  //To check whether the input box is focused
  const [focusing, setFocusing] = useState(false);

  const handleOnBlur = () => {
    setFocusing(true);
  };

  return (
    <div className="form-group mt-4 mb-4">
      {inputValues.type !== "select" ? (
        <TextField
          type={inputValues.type}
          fullWidth
          InputLabelProps={{ shrink: true }}
          label={inputValues.label}
          variant="standard"
          placeholder={inputValues?.placeHolder}
          required={inputValues?.required}
          value={inputValues.value ? inputValues.value : ""}
          focusing={focusing.toString()}
          onBlur={handleOnBlur}
          onChange={inputValues?.onChange}
          name={inputValues?.name}
          autoComplete="off"
        />
      ) : inputValues.type === "select" ? (
        <TextField
          select
          fullWidth
          InputLabelProps={{ shrink: true }}
          label={inputValues.label}
          variant="standard"
          placeholder={inputValues?.placeHolder}
          required={inputValues?.required}
          value={inputValues.value ? inputValues.value : " "}
          focusing={focusing.toString()}
          onBlur={handleOnBlur}
          onChange={inputValues?.onChange}
          name={inputValues?.name}
        >
          <MenuItem value=" ">--Select--</MenuItem>
          {inputValues.options.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
      ) : (
        // <select
        //   className="form-select"
        //   aria-label="Default select example"
        //   required={inputValues?.required}
        //   focusing={focusing.toString()}
        //   onBlur={handleOnBlur}
        //   id={inputValues?.name}
        //   onChange={inputValues?.onChange}
        //   name={inputValues?.name}
        //   defaultValue={inputValues?.value}
        // >
        //   <option value="">--Select--</option>
        //   {inputValues?.options.map((element) => {
        //     return <option value={element}>{element}</option>;
        //   })}
        // </select>
        ""
      )}
      <p className="form-error">
        <span>{inputValues?.errorMsg?.required}</span>
      </p>
    </div>
  );
};
