import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import React, { useState } from "react";
import { InputForm } from "../input";
import "./PaymentDialog.css";

export const PaymentDialog = ({
  open,
  onClose,
  paymentData = [],
  readOnly = false,
  printOnly = false,
  KOTPrintOnly = false,
}) => {
  let totalAmount = 0;
  let [paidBy, setPaidBy] = useState("Cash");
  let date =
    new Date().toISOString().split("T")[0] +
    " " +
    new Date().toLocaleString().split(",")[1].trim();

  // Onload get the total payment amount
  if (paymentData[0]?.orders.length > 0) {
    totalAmount = paymentData[0].orders.reduce(
      (prevValue, curValue) => prevValue + Number(curValue.price),
      0
    );
  }

  /***@function handleFormInputChange - Any of the input value changes */
  const handleFormInputChange = (e) => {
    setPaidBy(e.target.value);
  };

  let formInputs = [
    {
      name: "paidBy",
      type: "select",
      label: "Cash Paid By",
      placeHolder: "Cash Paid By",
      value: paidBy,
      options: ["Cash", "Card", "PhonePe", "Google Pay", "Paytm", "Amazon Pay"],
      onChange: handleFormInputChange,
    },
  ];

  /** @function handleOnSubmit - Handle on click pay bill button */
  const handleOnSubmit = () => {
    paymentData[0].date = date;
    paymentData[0].totalAmount = totalAmount;
    if (printOnly) {
      paymentData[0].printOnly = true;
    } else if (KOTPrintOnly) {
      paymentData[0].orders = paymentData[0].orders.map((e) => ({
        ...e,
        kot: { ...e.kot, quantity: 0, taken: true },
      }));
    } else {
      paymentData[0].paidBy = paidBy;
    }
    onClose("", paymentData[0]);
  };

  /**@function handleOnCancel - When the user clicks on cancel button  */
  const handleOnCancel = () => {
    onClose();
  };

  return (
    <Dialog className="paymentDialog" open={open} onClose={onClose}>
      <DialogTitle>
        {KOTPrintOnly
          ? "KOT Bill"
          : printOnly
          ? "Print Bill"
          : "Complete Payment"}
      </DialogTitle>
      <DialogContent className="p-1">
        <div className="non-printable">
          <table className="table table-borderless table-hover">
            <thead className="bg-light text-secondary">
              <tr>
                <th width="80%">Item Name</th>
                <th width="10%" className="text-end">
                  Qty
                </th>
                <th width="10%" className="text-end">
                  Price
                </th>
              </tr>
            </thead>
            <tbody>
              {paymentData[0]?.orders.length > 0 ? (
                paymentData[0].orders.map((el, index) => {
                  return (
                    <tr key={index}>
                      <td>{el.itemName}</td>
                      <td className="text-end">
                        {!KOTPrintOnly ? el.quantity : el.kot.quantity}
                      </td>
                      <td className="text-end">&#x20B9;{el.price}</td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={3}>
                    <div className="no-data text-center mt-5">
                      <p className="no-data-title">No Ordes Found</p>
                      <p className="no-data-subtitle">
                        Please select table number to see the orders.
                      </p>
                    </div>
                  </td>
                </tr>
              )}
            </tbody>
            <tfoot className="bg-danger text-light">
              <tr>
                <td colSpan={2}>Total</td>
                <td className="text-end">&#x20B9;{totalAmount}</td>
              </tr>
            </tfoot>
          </table>
          {readOnly && !printOnly && !KOTPrintOnly ? (
            ""
          ) : (
            <>
              {!printOnly && !KOTPrintOnly ? (
                <div className="form-elements">
                  {formInputs.map((element, index) => {
                    return (
                      <React.Fragment key={index}>
                        <div className="mt-1">
                          <InputForm inputValues={element} />
                        </div>
                      </React.Fragment>
                    );
                  })}
                </div>
              ) : (
                ""
              )}
              <div className="text-end mt-3 d-flex pe-1 ps-3">
                <button
                  className="btn btn-secondary me-2"
                  onClick={() => handleOnCancel()}
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary"
                  onClick={() => handleOnSubmit()}
                >
                  {KOTPrintOnly
                    ? "Print KOT"
                    : printOnly
                    ? "Print Bill"
                    : "Complete Payment"}
                </button>
              </div>
            </>
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};
