import React, { useEffect, useState } from "react";
import { InputForm } from "../../Shared/input";
import "./Login.css";
import logoImage from "./../../images/logo.png";
import { useDispatch, useSelector } from "react-redux";
import { setLoginData } from "../../Redux/Actions/LoginActions";
import { useNavigate } from "react-router-dom";
import { Loading } from "../../Shared/Loading/Loading";

export const Login = () => {
  document.title = "Triparva Restaurant - Login";
  let navigate = useNavigate();
  const [isSubmitted, setSubmitted] = useState(false);
  // Select the users data after successfull login
  let userData = useSelector((state) => state.loginReducers);
  // Dispatch an action for the login
  const dispatch = useDispatch();
  // Set form default values
  let [formStates, setFormsData] = useState({
    userName: "",
    password: "",
  });

  // Useeffect call for dependency reduce number of re rendering
  useEffect(() => {
    if (Object.keys(userData?.data).length > 0) {
      localStorage.setItem("loggedIn", true);
      setSubmitted(false);
      navigate("/admin");
    } else {
      localStorage.clear();
      if (isSubmitted && !userData.loading) {
        alert("Please enter correct username and password");
        setSubmitted(false);
        setFormsData({
          userName: "",
          password: "",
        });
      }
    }
  }, [userData, navigate, isSubmitted]);

  // Form input values changes
  const handleFormInputChange = (e) => {
    setFormsData({ ...formStates, [e.target.name]: e.target.value });
  };

  // Handle on submit form
  const onSubmitForm = (e) => {
    e.preventDefault();
    setSubmitted(true);
    dispatch(setLoginData(formStates));
  };

  let inputValues = [
    {
      name: "userName",
      type: "text",
      label: "Username",
      placeHolder: "Enter your Username",
      value: formStates["userName"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
      required: true,
    },
    {
      name: "password",
      type: "password",
      label: "Password",
      placeHolder: "Enter your Password",
      value: formStates["password"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
      required: true,
    },
  ];

  return (
    <>
      {userData.loading ? (
        <Loading />
      ) : (
        <div className="login-container mt-5">
          <div className="col-md-3 loginForm">
            <img src={logoImage} alt="Logo" className="logo" />
            <h1 className="header">Triparva Restaurant</h1>
            <form onSubmit={onSubmitForm} autoComplete="off">
              {inputValues.map((element, index) => {
                return (
                  <React.Fragment key={index}>
                    <InputForm inputValues={element} key={index} />
                  </React.Fragment>
                );
              })}
              <div className="text-end">
                <button className="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  );
};
