import { takeEvery, put, call } from "redux-saga/effects";
import { ActionConstants } from "./../Redux/Action-Constants/ActionConstants";
import axios from "axios";

const BASE_URL = "https://triparva-restaurant-backend-new-cxd5.vercel.app/v1/api/";

function fetchApi(params) {
  return axios
    .post(BASE_URL + params.url, params.payload)
    .then((response) => response)
    .catch((error) => error);
}

function getApi(params) {
  return axios
    .get(BASE_URL + params.url)
    .then((response) => response)
    .catch((error) => error);
}

function* getDashboardData() {
  let params = { url: "orders/everydaydata" };
  let userLogin = yield call(getApi, params);
  try {
    yield put({
      type: ActionConstants.DASHBOARD_DATA_SUCCESS,
      payload: userLogin["data"].data,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.DASHBOARD_DATA_ERROR,
      payload: error["data"],
    });
  }
}

function* getDashboardOrders(action) {
  let params = { url: "orders/findOrders", payload: action.payload };
  let userLogin = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.DASHBOARD_ORDERS_SUCCESS,
      payload: userLogin["data"].data,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.DASHBOARD_ORDERS_ERROR,
      payload: error["data"],
    });
  }
}
function* deleteDashboardOrder(action) {
  let params = { url: "orders/deleteorder", payload: action.payload };
  let userLogin = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.DASHBOARD_DELETE_SUCCESS,
      payload: userLogin["data"].data,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.DASHBOARD_DELETE_ERROR,
      payload: error["data"],
    });
  }
}

function* dashboardSaga() {
  yield takeEvery(ActionConstants.DASHBOARD_DATA, getDashboardData);
  yield takeEvery(ActionConstants.DASHBOARD_ORDERS, getDashboardOrders);
  yield takeEvery(ActionConstants.DASHBOARD_DELETE, deleteDashboardOrder);
}

export default dashboardSaga;
