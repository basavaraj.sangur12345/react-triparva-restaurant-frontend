import { all, fork } from "redux-saga/effects";
import BillingSaga from "./BillingSaga";
import dashboardSaga from "./DashboardSaga";
import ItemSaga from "./ItemSaga";
import LoginSaga from "./LoginSaga";

export default function* RootSaga() {
  yield all([
    fork(ItemSaga),
    fork(LoginSaga),
    fork(BillingSaga),
    fork(dashboardSaga),
  ]);
}
