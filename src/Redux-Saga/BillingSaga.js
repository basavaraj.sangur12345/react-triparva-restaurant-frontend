import { takeEvery, put, call, takeLatest } from "redux-saga/effects";
import { ActionConstants } from "./../Redux/Action-Constants/ActionConstants";
import axios from "axios";

const BASE_URL = "https://triparva-restaurant-backend-new-cxd5.vercel.app/v1/api/";

async function fetchApi(params) {
  return await axios
    .post(BASE_URL + params.url, params.payload)
    .then((response) => response)
    .catch((error) => error);
}

function getApi(params) {
  return axios
    .get(BASE_URL + params.url)
    .then((response) => response)
    .catch((error) => error);
}

// Saga for getting the invoice data information
function* loadInvoiceData() {
  let params = { url: "orders/getinvoiceid" };
  let itemsData = yield call(getApi, params);
  try {
    yield put({
      type: ActionConstants.LOAD_INVOICE_DATA_SUCCESS,
      payload: itemsData["data"],
    });
  } catch (error) {
    yield put({
      type: ActionConstants.LOAD_INVOICE_DATA_SUCCESS,
      payload: error,
    });
  }
}
// End saga

// Saga to check for table number whether is it exist or not
function* checkForTableNumber(action) {
  let params = {
    url: "orders/checkfortablenumber",
    payload: {
      tableNumber: action.payload,
    },
  };
  let tableData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.CHECK_FOR_TABLE_NUMBER_SUCCESS,
      payload: tableData["data"],
    });
  } catch (error) {
    yield put({
      type: ActionConstants.CHECK_FOR_TABLE_NUMBER_ERROR,
      payload: error,
    });
  }
}

// Saga to submit a new order
function* onSubmitNewOrder(action) {
  let params = {
    url: "orders/createorder",
    payload: action.payload,
  };
  let tableData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.SUBMIT_NEW_ORDER_SUCCESS,
      payload: tableData["data"],
    });
  } catch (error) {
    yield put({
      type: ActionConstants.SUBMIT_NEW_ORDER_ERROR,
      payload: error,
    });
  }
}

function* onLoadOrders() {
  let params = {
    url: "orders/getorders",
  };
  let tableData = yield call(getApi, params);
  try {
    yield put({
      type: ActionConstants.LOAD_ORDERS_SUCCESS,
      payload: tableData["data"],
    });
  } catch (error) {
    yield put({
      type: ActionConstants.LOAD_ORDERS_ERROR,
      payload: error,
    });
  }
}

function* findOrder(action) {
  let params = {
    url: "orders/findOrders",
    payload: action.payload,
  };
  let tableData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.FIND_ORDER_SUCCESS,
      payload: tableData["data"],
    });
  } catch (error) {
    yield put({
      type: ActionConstants.FIND_ORDER_ERROR,
      payload: error,
    });
  }
}

function* updateOrder(action) {
  let params = {
    url: "orders/updateorder",
    payload: action.payload,
  };
  let tableData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.UPDATE_ORDER_SUCCESS,
      payload: tableData["data"],
    });
  } catch (error) {
    yield put({
      type: ActionConstants.UPDATE_ORDER_ERROR,
      payload: error,
    });
  }
}

function* BillingSaga() {
  yield takeEvery(ActionConstants.LOAD_INVOICE_DATA, loadInvoiceData);
  yield takeLatest(ActionConstants.CHECK_FOR_TABLE_NUMBER, checkForTableNumber);
  yield takeLatest(ActionConstants.SUBMIT_NEW_ORDER, onSubmitNewOrder);
  yield takeLatest(ActionConstants.LOAD_ORDERS, onLoadOrders);
  yield takeLatest(ActionConstants.FIND_ORDER, findOrder);
  yield takeLatest(ActionConstants.UPDATE_ORDER, updateOrder);
}

export default BillingSaga;
