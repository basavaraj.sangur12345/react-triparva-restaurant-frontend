import { takeEvery, put, call } from "redux-saga/effects";
import { ActionConstants } from "./../Redux/Action-Constants/ActionConstants";
import axios from "axios";

const BASE_URL = "https://triparva-restaurant-backend-new-cxd5.vercel.app/v1/api/"

function fetchApi(params) {
  return axios
    .post(BASE_URL + params.url, params.payload)
    .then((response) => response)
    .catch((error) => error);
}

function getApi(params) {
  return axios
    .get(BASE_URL + params.url)
    .then((response) => response)
    .catch((error) => error);
}

function* loadItemsList() {
  let params = { url: "items/getitems" };
  let itemsData = yield call(getApi, params);
  try {
    yield put({
      type: ActionConstants.LOAD_ITEMSLIST_SUCCESS,
      payload: itemsData["data"].data,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.LOAD_ITEMSLIST_SUCCESS,
      payload: error["data"],
    });
  }
}

function* addItemList(action) {
  let params = { url: "items/createitem", payload: action.payload };
  let itemsData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.ADD_ITEMLIST_SUCCESS,
      payload: itemsData,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.ADD_ITEMLIST_ERROR,
      payload: error["data"],
    });
  }
}

function* getSingleItem(action) {
  let params = { url: "items/getsingleitem", payload: action.payload };
  let itemsData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.LOAD_SINGLE_ITEMSLIST_SUCCESS,
      payload: itemsData,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.LOAD_SINGLE_ITEMSLIST_ERROR,
      payload: error["data"],
    });
  }
}

function* deleteItemList(action) {
  let params = { url: "items/deleteitem", payload: action.payload };
  let itemsData = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.DELETE_ITEMLIST_SUCCESS,
      payload: itemsData,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.DELETE_ITEMLIST_ERROR,
      payload: error["data"],
    });
  }
}

function* ItemSaga() {
  yield takeEvery(ActionConstants.LOAD_ITEMSLIST, loadItemsList);
  yield takeEvery(ActionConstants.ADD_ITEMLIST, addItemList);
  yield takeEvery(ActionConstants.DELETE_ITEMLIST, deleteItemList);
  yield takeEvery(ActionConstants.LOAD_SINGLE_ITEMSLIST, getSingleItem);
}

export default ItemSaga;
