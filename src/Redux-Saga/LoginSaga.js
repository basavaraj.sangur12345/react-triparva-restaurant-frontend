import { takeEvery, put, call } from "redux-saga/effects";
import { ActionConstants } from "./../Redux/Action-Constants/ActionConstants";
import axios from "axios";

const BASE_URL = "https://triparva-restaurant-backend-new-cxd5.vercel.app/v1/api/"

function fetchApi(params) {
  return axios
    .post(BASE_URL + params.url, params.payload)
    .then((response) => response)
    .catch((error) => error);
}

function* loginUser(action) {
  let params = { url: "non-auth/userlogin", payload: action.payload };
  let userLogin = yield call(fetchApi, params);
  try {
    yield put({
      type: ActionConstants.LOAD_USERS_SUCCESS,
      payload: userLogin["data"].data,
    });
  } catch (error) {
    yield put({
      type: ActionConstants.LOAD_USERS_ERROR,
      payload: error["data"],
    });
  }
}

function* LoginSaga() {
  yield takeEvery(ActionConstants.LOAD_USERS, loginUser);
}

export default LoginSaga;
