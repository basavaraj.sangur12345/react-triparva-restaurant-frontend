import { Route, Routes, Navigate } from "react-router-dom";
import { Login } from "../Authentication/Login/Login";
import { Admin } from "../Pages/Admin/Admin";
import React from "react";

export const PublicRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to={"/authentication/login"} />} />
      <Route path="/authentication/login" element={<Login />}></Route>
      <Route path="/admin/*" element={<Admin />}></Route>
      <Route path="*" element={<h1>Path Not Found</h1>}></Route>
    </Routes>
  );
};
