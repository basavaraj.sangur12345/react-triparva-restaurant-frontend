import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { Billing } from "../Pages/Admin/Billing/Billing";
import { Dashboard } from "../Pages/Admin/Dashboard/dashboard";
import { Inventory } from "../Pages/Admin/Inventory/Inventory";
import { AddItem } from "../Pages/Admin/Items/Add-Item/Add-Item";
import { Items } from "../Pages/Admin/Items/Items";
import { Orders } from "../Pages/Admin/Orders/Orders";

export const ProtectedRoutes = ({ isLoggedIn }) => {
  return (
    <React.Fragment>
      {/* {isLoggedIn ? ( */}
      <Routes>
        <Route path="/" element={<Navigate to={"/admin/dashboard"} />} />
        <Route path="dashboard" element={<Dashboard />} />
        <Route path="items">
          <Route
            path="/items"
            element={<Navigate to="/admin/items/itemslist" replace />}
          />
          <Route path="itemslist" element={<Items />} />
          <Route path="additem" element={<AddItem />} />
          <Route path="edititem/:itemid" element={<AddItem />} />
        </Route>
        <Route path="billing" element={<Billing />} />
        <Route path="orders" element={<Orders />} />
        <Route path="inventory" element={<Inventory />} />
        <Route
          path="*"
          element={
            <>
              <p className="d-none">{(document.title = "Page Not Found!")}</p>
              <div className="container mt-3">
                <div className="col-md-8 ps-0">
                  <h1 className="section-header">Page Not Found</h1>
                </div>
              </div>
              <div className="no-data text-center mt-5">
                <p className="no-data-title">404</p>
                <p className="no-data-subtitle">Page Not Found.</p>
                <p className="no-data-subtitle">Contact Your Administrator.</p>
              </div>
            </>
          }></Route>
      </Routes>
      {/* ) : (
        <Navigate to="/" replace />
      )} */}
    </React.Fragment>
  );
};
