import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { findOrder } from "../../../Redux/Actions/BillingActions";
import { PaymentDialog } from "../../../Shared/Payment-Dialog/PaymentDialog";
import {
  deleteDashboardOrder,
  getDashboardOrders,
  getDataDashboard,
} from "../../../Redux/Actions/Dashboard";
import { Loading } from "../../../Shared/Loading/Loading";
import "./dashboard.css";

export const Dashboard = () => {
  document.title = "Triparva Restaurant - Dashboard";
  let date = new Date().toDateString();
  let dispatch = useDispatch();
  let dashboardReducer = useSelector((state) => state?.dashboardReducer);
  let overviewData = dashboardReducer?.dashboardData;
  let dashboardOrders = dashboardReducer?.dashboardOrders || [];
  let navigate = useNavigate();
  const [openPaymentDialog, setPaymentDialogOpen] = useState(false); //for the manual cash dialog open and close
  let [printOnly, setprintOnly] = useState(false);
  let [paymentInvoiceId, setPaymentInvoiceId] = useState("");
  let billingReducer = useSelector((state) => state?.billingReducer);
  let paymentDialogData = billingReducer?.findOrder?.data; //On selecting the table number reducer
  const [completePaymentData, setCompletePaymentData] = useState({}); // For editing the manual cash edit

  /** @getdashboardData - Get all the dashboard data */
  const getDashboardData = useCallback(() => {
    dispatch(getDataDashboard());
  }, [dispatch]);

  /**@function getTodaysOrders - Get all the orders information */
  const getTodaysOrders = useCallback(
    (startDate = new Date(), endDate = new Date()) => {
      startDate.setHours(0, 0, 0, 0);
      endDate.setHours(23, 59, 59, 99);
      let filter = {
        filter: {
          createdAt: {
            $gte: startDate,
            $lte: endDate,
          },
        },
      };
      dispatch(getDashboardOrders(filter));
    },
    [dispatch]
  );

  /** @function useMemo - function to set the initial values */
  useMemo(() => {
    if (overviewData) {
      overviewData["todaysRevenue"] = overviewData["orders"]?.find(
        (x) =>
          x._id.day === new Date().getDate() &&
          x._id.month === new Date().getMonth() + 1 &&
          x._id.year === new Date().getFullYear()
      );
    }

    if (paymentDialogData) {
      setPaymentInvoiceId(paymentDialogData);
    }
  }, [overviewData, paymentDialogData]);

  /** Useffect to load information for the first time */
  useEffect(() => {
    getDashboardData();
    getTodaysOrders(new Date(), new Date());
  }, [getDashboardData, getTodaysOrders]);

  // /**@function exportasexcel - Ecport orders to excel */
  // const exportAsExcel = () => {
  //   const myJsonString = JSON.stringify(dashboardOrders);
  //   const blob = new Blob([myJsonString], {
  //     type: "application/vnd.ms-excel;charset=utf-8",
  //   });
  //   saveAs(blob, "Report.xls");
  // };

  /** @funcion handlePaymentDialogOpen */
  const handlePaymentDialogOpen = useCallback(
    (invoiceIds, printOnlys) => {
      dispatch(findOrder({ filter: { _id: invoiceIds } }));
      if (printOnlys) {
        setprintOnly(true);
      } else {
        setprintOnly(false);
      }
      setPaymentDialogOpen(true);
    },
    [setPaymentDialogOpen, dispatch]
  );

  /** @funcion handlePaymentDialogClose */
  const handlePaymentDialogClose = (event, _completePaymentData) => {
    if (_completePaymentData && _completePaymentData !== "backdropClick") {
      setCompletePaymentData(_completePaymentData);
      setPaymentDialogOpen(false);
      setTimeout(() => {
        window.print();
      }, 100);

      window.onafterprint = () => {
        setTimeout(() => {
          resetAll(); // Reset all fields
        }, 1000);
      };
    } else {
      setPaymentDialogOpen(false);
    }
  };

  const resetAll = () => {
    setCompletePaymentData({}); // Set complete payment data to initial
    setPaymentInvoiceId("");
    setPaymentDialogOpen(false);
    setprintOnly(false);
  };

  const handleOnDeleteClick = (_id) => {
    dispatch(deleteDashboardOrder({ _id: _id }));
    getDashboardData();
    getTodaysOrders(new Date(), new Date());
  };

  return (
    <>
      {!dashboardReducer?.loading ? (
        <>
          <div className="container mt-3">
            <div className="col-md-8 ps-0">
              <h1 className="section-header">Dashboard</h1>
              <small className="text-muted">{date}</small>
            </div>
            <div className="row mt-5">
              <h1 className="section-header section-subheader mb-3">
                Overview
              </h1>
              <div className="row">
                <div className="col-md-3 dt-card">
                  <p className="dt-icon material-icons material-symbols-outlined">
                    account_balance_wallet
                  </p>
                  <div className="dt-card-info">
                    <p className="number">
                      &#x20B9;{overviewData?.todaysRevenue?.totalPrice || 0}.00
                    </p>
                    <p className="dt-title">Total Revenue</p>
                  </div>
                </div>
                <div
                  className="col-md-3 dt-card"
                  onClick={() => navigate("/admin/items")}>
                  <p className="dt-icon material-icons material-symbols-outlined">
                    assignment
                  </p>
                  <div className="dt-card-info">
                    <p className="number">146</p>
                    <p className="dt-title">Total Menus</p>
                  </div>
                </div>
                <div
                  className="col-md-3 dt-card"
                  onClick={() => navigate("/admin/orders")}>
                  <p className="dt-icon material-icons material-symbols-outlined">
                    shopping_bag
                  </p>
                  <div className="dt-card-info">
                    <p className="number">
                      {overviewData?.totalOrders < 10
                        ? "0" + overviewData?.totalOrders
                        : overviewData?.totalOrders || 0}
                    </p>
                    <p className="dt-title">Completed Orders</p>
                  </div>
                </div>
                <div
                  className="col-md-3 dt-card"
                  onClick={() => navigate("/admin/billing")}>
                  <p className="dt-icon material-icons material-symbols-outlined">
                    pending_actions
                  </p>
                  <div className="dt-card-info">
                    <p className="number">
                      00
                      {/* {overviewData?.totalOrders < 10
      ? "0" + overviewData?.totalOrders
      : overviewData?.totalOrders || 0} */}
                    </p>
                    <p className="dt-title">Pending Orders</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-5">
              <div className="col-md-9">
                <div className="row p-0">
                  <div className="col-md-9 p-0">
                    <h1 className="section-header section-subheader">
                      Todays Orders
                    </h1>
                  </div>
                  <div className="col-md-3 p-0">
                    <button className="btn btn-secondary">
                      <span className="material-icons material-icons-outlined">
                        file_download
                      </span>
                      &nbsp; Export To Excel
                    </button>
                  </div>
                </div>
                <div className="table-container mt-3">
                  {dashboardOrders.length > 0 ? (
                    <>
                      <table className="table tabe-responsive table-bordered table-hover">
                        <thead className="bg-light text-secondary">
                          <tr>
                            <th>Invoice Id</th>
                            <th>Table Number</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Cash Paid</th>
                            <th>Paid By</th>
                            <th>Bill</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {dashboardOrders.map((el, index) => {
                            return (
                              <tr key={index}>
                                <td>{el.invoiceId}</td>
                                <td>{el.tableNumber}</td>
                                <td>
                                  {new Date(el.updatedAt)
                                    .toISOString()
                                    .split("T")[0] +
                                    " " +
                                    new Date(el.updatedAt)
                                      .toLocaleString()
                                      .split(",")[1]
                                      .trim()}
                                </td>
                                <td className="text-end">
                                  &#x20B9;{el.total ? +el.total : "0"}
                                </td>
                                <td className="fw-bold text-success">
                                  {el.cashPaid ? el.cashPaid : "-"}
                                </td>
                                <td className="fw-bold text-info">
                                  {el.paidBy ? el.paidBy : "-"}
                                </td>
                                <td
                                  className="fw-bold text-success table-buttons"
                                  onClick={() =>
                                    handlePaymentDialogOpen(el._id, false)
                                  }>
                                  Re-Print
                                </td>
                                <td
                                  className="fw-bold text-danger table-buttons"
                                  onClick={() =>
                                    handleOnDeleteClick(el._id, false)
                                  }>
                                  Delete
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                      <PaymentDialog
                        open={openPaymentDialog}
                        onClose={handlePaymentDialogClose}
                        paymentData={paymentInvoiceId}
                        printOnly={printOnly}
                        KOTPrintOnly={false}
                      />
                    </>
                  ) : (
                    <div className="no-data text-center mt-5">
                      <p className="no-data-title">No Orders Found!</p>
                      <p className="no-data-subtitle">
                        Create orders in billing to visible here.
                      </p>
                    </div>
                  )}
                </div>
              </div>
              <div className="col-md-3">
                <h1 className="section-header section-subheader">
                  Cancelled Orders
                </h1>
                <div className="no-data text-center mt-5">
                  <p className="no-data-title">No Cancelled Orders Found!</p>
                  <p className="no-data-subtitle">
                    Cancelled orders will be visible here.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <>
            {Object.keys(completePaymentData).length > 0 ? (
              <>
                {/*--------------------------------- Print section ------------------- */}
                <div id="print-section">
                  <div className="print-top-container">
                    <p className="printTitle">TRIPARVA RESTAURANT</p>
                    <p className="printSub-title">
                      Veg & Non-Veg Family Restaurant
                    </p>
                    <p className="print-address">
                      PB Road, Beside Ayurvedic College, Haveri
                    </p>
                    <p className="print-address">Mobile: +91 74837 40020</p>
                  </div>
                  <div className="memo-container">
                    <div className="container-information">
                      <div className="row">
                        <div className="col-2">
                          T.No:&nbsp;{completePaymentData.tableNumber}
                        </div>
                        <div className="col-10 text-end">
                          S.Name:&nbsp;{completePaymentData.supplierName}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="memo-container">
                    <div className="container-information">
                      <div className="row">
                        <div className="col-3">
                          {completePaymentData.invoiceId}
                        </div>
                        <div className="col-9 text-end">
                          {completePaymentData.date}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="itemsList-container">
                    <table className="table-order">
                      <thead>
                        <tr>
                          <th className="text-start">Particulars</th>
                          <th className="text-end">Qty</th>
                          <th className="text-end">Rate</th>
                          <th className="text-end">Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        {completePaymentData.orders.map((el, index) => {
                          return (
                            <tr key={index}>
                              <td>{el.itemName}</td>
                              <td className="text-end">{el.quantity}</td>
                              <td className="text-end">{el.singlePrice}</td>
                              <td className="text-end">&#x20B9;{el.price}</td>
                            </tr>
                          );
                        })}
                        <tr>
                          <th className="text-start">Total</th>
                          <th colSpan="3" className="text-end">
                            &#x20B9;{completePaymentData.totalAmount}
                          </th>
                        </tr>
                      </tbody>
                    </table>
                    <p className="thanks">ನಮ್ಮ ತ್ರಿಪರ್ವ</p>
                    <p className="thanks">ಧನ್ಯವಾದಗಳು. ಪುನಃ ಭೇಟಿ ನೀಡಿ.</p>
                    <p className="thanks-insta">
                      <svg
                        style={{ width: "14px", height: "14px" }}
                        viewBox="0 0 24 24">
                        <path
                          fill="currentColor"
                          d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z"
                        />
                      </svg>{" "}
                      namma_triparva
                    </p>
                  </div>
                </div>
                {/* -------------------------------- Print section ends ---------------- */}
              </>
            ) : (
              ""
            )}
          </>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};
