import React from "react";
import "./Inventory.css";

export const Inventory = () => {
  document.title = "Triparva Restaurant - Inventory";
  return (
    <>
      <div className="container mt-3">
        <div className="col-md-8 ps-0">
          <h1 className="section-header">Inventory</h1>
        </div>
      </div>
      <div className="no-data text-center mt-5">
        <p className="no-data-title">Development Inprogress!</p>
        <p className="no-data-subtitle">
          Page is under development, will come back soon!.
        </p>
      </div>
    </>
  );
};
