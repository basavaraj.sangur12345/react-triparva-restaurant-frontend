import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import { addItems, getsingleitem } from "../../../../Redux/Actions/ItemActions";
import { InputForm } from "../../../../Shared/input";
import { Loading } from "../../../../Shared/Loading/Loading";

export const AddItem = () => {
  document.title = "Triparva Restaurant - Add Item";
  const [formStates, setFormsData] = useState({
    itemName: "",
    price: "",
  });
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let addedItem = useSelector((state) => state?.itemsReducer);
  let pageTitle = "Add Item";

  // Form input values changes
  const handleFormInputChange = (e) => {
    setFormsData({ ...formStates, [e.target.name]: e.target.value });
  };

  //   Form Inputs
  let formInputs = [
    {
      name: "itemName",
      type: "text",
      label: "Item Name",
      placeHolder: "Enter your Item Name",
      value: formStates["itemName"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
      required: true,
    },
    {
      name: "price",
      type: "number",
      label: "Item Price",
      placeHolder: "Enter your Item Price",
      value: formStates["price"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
      required: true,
    },
  ];

  // on form submission
  const onSubmitForm = (e) => {
    e.preventDefault();
    dispatch(addItems(formStates));
    if (!addedItem?.loading) {
      alert("Product has been added successfully!");
      navigate("/admin/items");
    }
  };

  // Edit will starts from here
  const { itemid } = useParams();

  if (itemid) {
    document.title = "Edit Item";
    pageTitle = "Edit Item";
  }

  useEffect(() => {
    if (itemid) {
      const getSingelItem = () => {
        dispatch(getsingleitem({ _id: itemid }));
      };
      getSingelItem();

      // if (!addedItem.loading && addedItem?.itemDetails) {
      //   Number(addedItem?.itemDetails?.data.data.price);
      //   setFormsData((current) => ({
      //     ...current,
      //     ...addedItem?.itemDetails?.data.data,
      //   }));
      // }
    }
  }, [itemid, dispatch]);

  return (
    <>
      {addedItem.loading ? <Loading /> : ""}
      <section className="add-item container mt-3">
        <div className="row">
          <div className="col-md-8">
            <h1 className="section-header">{pageTitle}</h1>
          </div>
        </div>
        <div className="form-container">
          <form onSubmit={onSubmitForm} autoComplete="off">
            <div className="row">
              {formInputs.map((element, index) => {
                return (
                  <React.Fragment key={index}>
                    <div className="col-md-6">
                      <InputForm inputValues={element} />
                    </div>
                  </React.Fragment>
                );
              })}
            </div>
            <div className="text-end pe-2">
              <button className="btn btn-primary">Submit</button>
              <Link to={"/admin/items"} className="btn btn-danger ms-1">
                Cancel
              </Link>
            </div>
          </form>
        </div>
      </section>
    </>
  );
};
