import { TablePagination } from "@mui/material";
import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { ActionConstants } from "../../../Redux/Action-Constants/ActionConstants";
import { deleteItems, loadItemList } from "../../../Redux/Actions/ItemActions";
import { Loading } from "../../../Shared/Loading/Loading";
import "./Items.css";

export const Items = () => {
  // Get all the items list from the reducer
  document.title = "Triparva Restaurant - Items";
  let items = useSelector((state) => state?.itemsReducer);
  const [search, setSearch] = useState("");

  // use dispatch hook to dispatch an action to get list of items
  let dispatch = useDispatch();

  // on Search information
  const onSearchChange = useMemo(() => {
    if (!search) return items.itemsList;
    else {
      return [
        ...items.itemsList.filter((items) =>
          items.itemName.toLowerCase().includes(search.toLowerCase())
        ),
      ];
    }
  }, [search, items]);

  useEffect(() => {
    dispatch(loadItemList());
  }, [dispatch]);

  /** Pagination items */
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // on click delete
  const onDeleteClick = (_id) => {
    if (window.confirm("Are you sure you want delete this item?")) {
      dispatch(deleteItems({ _id: _id }));
      if (!items?.loading) {
        dispatch(loadItemList(ActionConstants.LOAD_ITEMSLIST));
      }
    }
  };

  return (
    <section className="itemsList">
      {items.loading ? (
        <Loading />
      ) : (
        <>
          <div className="row container p-0 mt-3">
            <div className="col-md-8 ps-0">
              <h1 className="section-header">Items</h1>
            </div>
            <div className="col-md-4 pe-0 text-end d-flex align-items-center justify-content-end">
              <div className="row">
                <div className="col-md-7 px-0">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search for items name..."
                    aria-label="Search for items name..."
                    onChange={(e) => setSearch(e.target.value)}
                  />
                </div>
                <div className="col-md-5">
                  <Link
                    to={"/admin/items/additem"}
                    className="btn btn-secondary"
                  >
                    <span className="material-icons material-symbols-outlined">
                      receipt_long
                    </span>
                    &nbsp; Add Item
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-3 container p-0">
            {onSearchChange && onSearchChange.length > 0 ? (
              <>
                <table className="table table-bordered">
                  <thead className="bg-primary text-light">
                    <tr>
                      <th>SI.No</th>
                      <th>Item Name</th>
                      <th>Price</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {onSearchChange
                      .slice(page * rowsPerPage, rowsPerPage * (page + 1))
                      .map((element, index) => {
                        return (
                          <tr key={index}>
                            <th>{index + 1 + page * rowsPerPage}</th>
                            <td>{element.itemName}</td>
                            <td className="text-end">&#8377;{element.price}</td>
                            <td className="text-center">
                              <Link
                                to={`/admin/items/edititem/${element._id}`}
                                className="link link-success"
                              >
                                Edit
                              </Link>
                            </td>
                            <td className="text-center">
                              <Link
                                to={"#"}
                                className="link link-danger"
                                onClick={() => onDeleteClick(element._id)}
                              >
                                Delete
                              </Link>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
                <TablePagination
                  component="div"
                  showFirstButton
                  showLastButton
                  count={onSearchChange?.length}
                  page={page}
                  onPageChange={handleChangePage}
                  rowsPerPage={rowsPerPage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              </>
            ) : (
              ""
            )}
          </div>
        </>
      )}
    </section>
  );
};
