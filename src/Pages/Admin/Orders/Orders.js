import { TablePagination } from "@mui/material";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { findOrder } from "../../../Redux/Actions/BillingActions";
import { getDashboardOrders } from "../../../Redux/Actions/Dashboard";
import { InputForm } from "../../../Shared/input";
import { Loading } from "../../../Shared/Loading/Loading";
import { PaymentDialog } from "../../../Shared/Payment-Dialog/PaymentDialog";
import "./Orders.css";

export const Orders = () => {
  document.title = "Triparva Restaurant - Orders";
  let dispatch = useDispatch();
  //Default ordersfilters state
  const initalFilterState = {
    startDate: "",
    endDate: "",
    invoiceId: "",
    paidBy: "",
  };
  const [filterFormState, setFilterFormStates] = useState(initalFilterState);
  const allOrders = useSelector((state) => state?.dashboardReducer);
  const state = useSelector((state) => state);
  const [openPaymentDialog, setPaymentDialogOpen] = useState(false); //for the payment dialog open and close
  const [ordersViewData, setordersViewData] = useState("");

  // Form input values changes
  const handleFormInputChange = (e) => {
    setFilterFormStates({
      ...filterFormState,
      [e.target.name]: e.target.value,
    });
  };

  let formInputs = [
    {
      name: "startDate",
      type: "date",
      label: "Start Date",
      placeHolder: "Enter your Start Date",
      value: filterFormState["startDate"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
    },
    {
      name: "endDate",
      type: "date",
      label: "End Date",
      placeHolder: "Enter your End Date",
      value: filterFormState["endDate"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
    },
    {
      name: "invoiceId",
      type: "text",
      label: "Invoice Id",
      placeHolder: "Enter your Invoice Id",
      value: filterFormState["invoiceId"],
      onChange: handleFormInputChange,
      errorMsg: {
        required: "This field is required",
      },
    },
    {
      name: "paidBy",
      type: "select",
      label: "Cash Paid By",
      placeHolder: "Cash Paid By",
      value: filterFormState["paidBy"],
      options: ["Cash", "Card", "PhonePe", "Google Pay", "Paytm", "Amazon Pay"],
      onChange: handleFormInputChange,
    },
  ];

  /**@function loadAllOrders = Load all orders */
  const loadAllOrders = useCallback(() => {
    dispatch(getDashboardOrders({ filter: {} }));
  }, [dispatch]);

  /**@function loadOrdersOnViewClick - Load all the order on click of view */
  const loadOrdersOnViewClick = useCallback(
    (invoiceIds) => {
      dispatch(findOrder({ filter: { invoiceId: invoiceIds } }));
      setPaymentDialogOpen(true);
    },
    [setPaymentDialogOpen, dispatch]
  );

  /**@function handleOnDialogClose - for handling closing the dialog box */
  const handleOnDialogClose = () => {
    setPaymentDialogOpen(false);
  };

  /**@function useMemo - to handle the values after dispatching the values */
  useMemo(() => {
    setordersViewData(state?.billingReducer?.findOrder?.data);
  }, [state.billingReducer.findOrder]);

  // useEffect load orders
  useEffect(() => {
    loadAllOrders();
  }, [loadAllOrders]);

  /** Pagination items */
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // on Submission of filter
  const onSubmitFilter = (e) => {
    e.preventDefault();
    let filter = {};

    if (filterFormState.startDate && filterFormState.endDate) {
      filter = {
        createdAt: {
          $gte: new Date(
            new Date(filterFormState.startDate).setHours(0, 0, 0, 0)
          ),
          $lte: new Date(
            new Date(filterFormState.endDate).setHours(23, 59, 59, 99)
          ),
        },
      };
    }

    if (filterFormState.invoiceId) {
      filter["invoiceId"] = filterFormState.invoiceId;
    }

    if (filterFormState.paidBy) {
      filter["paidBy"] = filterFormState.paidBy;
    }

    if (Object.keys(filter).length > 0) {
      dispatch(getDashboardOrders({ filter }));
    }
  };

  const onClickCancel = () => {
    setFilterFormStates(initalFilterState);
    loadAllOrders();
  };

  return (
    <section className="orders-list">
      {allOrders?.loading || state?.billingReducer?.loading ? (
        <Loading />
      ) : (
        <div className="container mt-3">
          <div className="row">
            <div className="col-md-8 p-0">
              <div className="row">
                <div className="col-md-6 d-flex align-items-center ps-0">
                  <h1 className="section-header">Orders</h1>
                </div>
                <div className="col-md-6 pe-0">
                  <button className="btn btn-secondary">
                    <span className="material-icons material-icons-outlined">
                      file_download
                    </span>
                    &nbsp; Export To Excel
                  </button>
                </div>
              </div>
              {allOrders?.dashboardOrders.length > 0 ? (
                <>
                  <table className="table tabe-responsive table-bordered table-hover mt-3">
                    <thead className="bg-primary text-light">
                      <tr>
                        <th>Invoice Id</th>
                        <th>Table Number</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th className="text-center">Status</th>
                        <th className="text-center">Paid By</th>
                        <th className="text-center">View Orders</th>
                      </tr>
                    </thead>
                    <tbody>
                      {allOrders?.dashboardOrders
                        .slice(page * rowsPerPage, rowsPerPage * (page + 1))
                        .map((el, index) => {
                          return (
                            <tr key={index}>
                              <td>{el.invoiceId}</td>
                              <td>{el.tableNumber}</td>
                              <td>
                                {new Date(el.updatedAt)
                                  .toISOString()
                                  .split("T")[0] +
                                  " " +
                                  new Date(el.updatedAt)
                                    .toLocaleString()
                                    .split(",")[1]
                                    .trim()}
                              </td>
                              <td className="text-end">
                                &#x20B9;{el.total ? +el.total : "0"}
                              </td>
                              <td>
                                <span
                                  className={`${
                                    el.cashPaid && el.cashPaid === "Paid"
                                      ? "badge text-bg-primary bg-success rounded-pill w-100"
                                      : "badge text-bg-primary bg-danger rounded-pill w-100"
                                  }`}
                                >
                                  {el.cashPaid ? el.cashPaid : "-"}
                                </span>
                              </td>
                              <td className="fw-bold text-info">
                                {el.paidBy ? el.paidBy : "-"}
                              </td>
                              <td className="fw-bold text-success text-center">
                                <span
                                  className="table-buttons text-danger text-center"
                                  onClick={() =>
                                    loadOrdersOnViewClick(el.invoiceId)
                                  }
                                >
                                  View
                                </span>
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                  <TablePagination
                    component="div"
                    showFirstButton
                    showLastButton
                    count={allOrders?.dashboardOrders?.length}
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                  />
                </>
              ) : (
                <div className="no-data text-center mt-5">
                  <p className="no-data-title">No Orders Found!</p>
                  <p className="no-data-subtitle">
                    Create orders in billing to visible here.
                  </p>
                </div>
              )}
            </div>
            <div className="col-md-4">
              <h1 className="section-header">Filter</h1>
              <div className="mt-4">
                <div className="border rounded pt-2 pb-2 pe-3 ps-3">
                  <form onSubmit={(e) => onSubmitFilter(e)}>
                    {formInputs.map((element, index) => {
                      return (
                        <React.Fragment key={index}>
                          <div className="mt-1">
                            <InputForm inputValues={element} />
                          </div>
                        </React.Fragment>
                      );
                    })}
                    <div className="row">
                      <div className="col-md-6 p-0">
                        <button
                          type="reset"
                          className="btn btn-secondary w-100 pt-2 pb-2"
                          onClick={(e) => onClickCancel()}
                        >
                          Cancel
                        </button>
                      </div>
                      <div className="col-md-6 pe-0">
                        <button
                          type="submit"
                          className="btn btn-primary w-100 pt-2 pb-2"
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <PaymentDialog
            open={openPaymentDialog}
            onClose={handleOnDialogClose}
            paymentData={ordersViewData}
            readOnly={true}
          />
        </div>
      )}
    </section>
  );
};
