// KOT Billing in progress
// TODO: restrict with old quantity

import { Autocomplete, TextField } from "@mui/material";
import React, { useCallback, useMemo, useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  billingResetAll,
  findOrder,
  loadInvoiceData,
  onActionSelectTableNumber,
  onLoadOrders,
  onSaveOrder,
  updateOrder,
} from "../../../Redux/Actions/BillingActions";
import { loadItemList } from "../../../Redux/Actions/ItemActions";
import { InputForm } from "../../../Shared/input";
import { Loading } from "../../../Shared/Loading/Loading";
import { ManualCashForm } from "../../../Shared/Manual-Cash/ManualCashForm";
import { PaymentDialog } from "../../../Shared/Payment-Dialog/PaymentDialog";
import "./Billing.css";

export const Billing = () => {
  document.title = "Triparva Restaurant - Billing";
  // Billign default form states
  const initialBillingFormStates = {
    invoiceId: "",
    customerName: "",
    customerPhoneNumber: "",
    tableNumber: "",
    supplierName: "",
    total: 0,
  };
  const [billingFormStates, setBillingFormStates] = useState(
    initialBillingFormStates
  );

  //Order form default values
  const [orderFormStates, setOrderFormStates] = useState([]);

  /***@function handleFormInputChange - Any of the input value changes */
  const handleFormInputChange = (e) => {
    setBillingFormStates({
      ...billingFormStates,
      [e.target.name]: e.target.value,
    });

    // When the use change the table number selection
    if (e.target.name === "tableNumber") {
      onSelectTableNumber(e.target.value);
    }
  };

  // Invoice input elements
  let invoiceInputs = [
    {
      name: "invoiceId",
      type: "text",
      label: "Invoice Id",
      placeHolder: "Invoice Id",
      value: billingFormStates["invoiceId"],
      onChange: handleFormInputChange,
    },
    {
      name: "tableNumber",
      type: "select",
      label: "Table Number",
      placeHolder: "Table Number",
      value: billingFormStates["tableNumber"],
      options: [
        "R1",
        "R2",
        "R3",
        "R4",
        "R5",
        "R6",
        "R7",
        "R8",
        "R9",
        "R10",
        "R11",
        "R12",
        "H1",
        "H2",
        "H3",
        "H4",
        "H5",
        "H6",
        "H7",
        "H8",
        "H9",
        "H10",
        "H11",
        "H12",
        "G1",
        "G2",
        "G3",
        "G4",
        "G5",
        "G6",
        "G7",
        "G8",
        "G9",
        "G10",
      ],
      onChange: handleFormInputChange,
      required: true,
    },
    {
      name: "supplierName",
      type: "text",
      label: "Supplier Name",
      placeHolder: "Supplier Name",
      value: billingFormStates["supplierName"],
      onChange: handleFormInputChange,
    },
  ];

  /**
   * Billing functionality starts here.
   * all the variable declared after this which are needed for
   * this functionality
   */
  const dispatch = useDispatch();

  // Auto complete options
  let menuItemsList =
    useSelector((state) => state?.itemsReducer?.itemsList) || []; //Load all the items from item reducer

  const defaultProps = {
    options: menuItemsList,
    getOptionLabel: (option) => option?.itemName,
  };

  // Get all the states which are available in the billing reducer
  let billingReducer = useSelector((state) => state?.billingReducer);
  let invoiceId = billingReducer?.invoice?.invoiceId; //On generating the invoice Id
  let tableData = billingReducer?.tableData?.data; //On selecting the table number reducer
  let paymentDialogData = billingReducer?.findOrder?.data; //On selecting the table number reducer
  let allOrders = billingReducer?.orders?.data || []; //On selecting the table number reducer
  let totalPrice = 0; // To compute the total amount
  const [openManualCashDialog, setManualCashDialogOpen] = useState(false); //for the manual cash dialog open and close
  const [openPaymentDialog, setPaymentDialogOpen] = useState(false); //for the manual cash dialog open and close
  const [manualCashEditableData, setManualCashEditableData] = useState(""); // For editing the manual cash edit
  const [completePaymentData, setCompletePaymentData] = useState({}); // For editing the manual cash edit
  let [paymentInvoiceId, setPaymentInvoiceId] = useState("");
  let [printOnly, setprintOnly] = useState(false);
  let [KOTprintOnly, setKOTprintOnly] = useState(false);

  // need to treplace this code in the onload data
  if (orderFormStates.length > 0) {
    let initialValue = 0;
    totalPrice = orderFormStates.reduce(
      (acValue, currentValue) => acValue + Number(currentValue.price),
      initialValue
    );
  }

  /** @function loadInvoiceData */
  const loadInvoiceId = useCallback(() => {
    dispatch(loadInvoiceData());
  }, [dispatch]);

  /** @function loadMenuItems */
  const loadMenuItems = useCallback(() => {
    dispatch(loadItemList());
  }, [dispatch]);

  /** @function loadOrders */
  const loadOrders = useCallback(() => {
    dispatch(onLoadOrders());
  }, [dispatch]);

  /**  @function reactHookUseMemo
   *  @description If the order is already exist show it in the table data
   *  otherwise generate new invoice Id
   */
  useMemo(() => {
    if (tableData) {
      // Set primary form data which is invoice id and other
      setBillingFormStates((state) => ({
        ...tableData,
      }));

      // Set orders table data which is itemname, qty and price
      setOrderFormStates((state) => [...tableData.orders]);
    } else {
      setBillingFormStates((state) => ({
        ...state,
        supplierName: "",
        invoiceId: invoiceId,
      }));

      // Set orders table data which is itemname, qty and price
      setOrderFormStates((state) => []);
    }

    if (paymentDialogData) {
      setPaymentInvoiceId(paymentDialogData);
    }
  }, [tableData, invoiceId, paymentDialogData]);

  /**@function onSelectTableNumber - On selcting the table number */
  const onSelectTableNumber = (tableNumber) => {
    setOrderFormStates([]);
    dispatch(billingResetAll());
    dispatch(onActionSelectTableNumber(tableNumber));
  };

  /** @function onChangeQuantity - On changing of the quantity */
  const onChangeQuantity = (index, e) => {
    let orderFormValues = [...orderFormStates];
    orderFormValues[index][e.target.name] = e.target.value;
    let item = { ...orderFormValues[index] };
    // Update the quantity and price on change of quantity
    if (e.target.value) {
      let findItem = menuItemsList.find(
        (el) => el.itemName === item.itemName && item.cashAdded
      );
      // on change quantity for the manual cash edit
      if (findItem) {
        item.price = Number(item.quantity) * Number(findItem.price);
      } else {
        item.price = Number(item.quantity) * Number(item.singlePrice);
      }
    }
    orderFormValues[index] = { ...item };
    setOrderFormStates(orderFormValues);
  };

  // Calculate KOT Dialog information
  const calculateKOT = (index) => {
    let orderFormValues = [...orderFormStates];
    let item = { ...orderFormValues[index] };
    // KOT Starts
    if (!item?.kot.taken) {
      if (item.kot.oldquantity < item.quantity) {
        item.kot.quantity =
          Number(item.quantity) -
          Number(item.kot.oldquantity) +
          Number(item.kot.quantity);
      } else {
        item.kot.quantity = Number(item.quantity);
      }
    } else {
      if (Number(item.quantity) > Number(item.kot.oldquantity)) {
        item.kot.quantity =
          Number(item.quantity) -
          Number(item.kot.oldquantity) +
          Number(item.kot.quantity);
      } else {
        let value = item.kot.oldquantity - item.kot.quantity;
        let valueInfo = item.quantity - value;
        if (valueInfo > 0) {
          item.kot.quantity = value;
        } else {
          item.kot.quantity = 0;
        }
      }
      item.kot.oldquantity = item.quantity;
    }
    orderFormValues[index] = { ...item };
    setOrderFormStates(orderFormValues);
    // KOT Ends
  };

  /** @function handleOnDeleteItems - on deleting items from the orders table */
  const handleOnDeleteItems = (index) => {
    let orderFormValues = [...orderFormStates];
    orderFormValues = orderFormValues.filter((e, elIndex) => elIndex !== index);
    setOrderFormStates(orderFormValues);
  };

  /** @function handleOnSelectionOfItems - On selecting an item from the  */
  const handleOnSelectionOfItems = (event, selectedItem = "") => {
    if (selectedItem) {
      let orderFormValues = [...orderFormStates];
      let checkItemExistInOrder = orderFormValues.findIndex(
        (e) => e.itemName === selectedItem.itemName
      );
      // If the order is already exist in the order table increase the quantity otherwise add as new row
      if (checkItemExistInOrder !== -1) {
        console.log(selectedItem);
        orderFormValues[checkItemExistInOrder].quantity =
          orderFormValues[checkItemExistInOrder].quantity + 1;
        orderFormValues[checkItemExistInOrder].price =
          orderFormValues[checkItemExistInOrder].quantity * selectedItem.price;
        setOrderFormStates(orderFormValues);
      } else {
        selectedItem["quantity"] = 1;
        selectedItem["kot"] = { quantity: 1, oldquantity: 1, taken: false };
        selectedItem["singlePrice"] = selectedItem.price;
        orderFormValues.push(selectedItem);
        setOrderFormStates(orderFormValues);
      }
    }

    // Clear autocomplete text field.
    setTimeout(() => {
      if (
        document.getElementsByClassName("MuiAutocomplete-clearIndicator")[0]
      ) {
        document.getElementsByClassName(
          "MuiAutocomplete-clearIndicator"
        )[0].id = "ClearButton";
        document.getElementById("ClearButton").click();
      }
    }, 10);
  };

  /** @function useEffect */
  useEffect(() => {
    // Dispatch an action for loading invoice data
    loadInvoiceId();
    // Dispatch an action for loading all menu items
    loadMenuItems();
    // Load all orders
    loadOrders();

    // Component clean up function
    return () => {
      setBillingFormStates((state) => ({ ...state, tableNumber: " " }));
      setOrderFormStates([]);
      setCompletePaymentData({}); // Set complete payment data to initial
      setPaymentInvoiceId("");
      // end
      setprintOnly(false);
      setKOTprintOnly(false);
      dispatch(billingResetAll());
    };
  }, [loadInvoiceId, loadMenuItems, loadOrders, dispatch]);

  /*****************************************
   *  Manual Cash Price Handling starts Here
   * *****************************************/
  /**@function handleManualCashDialogOpen - for setting open and close for the -1 index not for edit  */
  const handleManualCashDialogOpen = (index) => {
    if (index !== -1) {
      let orderFormValues = [...orderFormStates];
      orderFormValues[index].index = index; // Add the index value for catch in handledialogonclose
      setManualCashEditableData(orderFormValues[index]);
    }
    setManualCashDialogOpen(true);
  };

  /**@function handleOnDialogClose - Once the user clicks on the dialog or cancel or overlay from the manual cash dialog box */
  const handleOnDialogClose = (manualCashFormValues) => {
    if (manualCashFormValues) {
      let orderFormValues = [...orderFormStates];
      // Check whether the edit index is exist or not, if exist modify the values else add new one.
      if (!manualCashFormValues.hasOwnProperty("index")) {
        orderFormValues.push(manualCashFormValues);
      } else {
        orderFormValues[manualCashFormValues.index] = manualCashFormValues;
      }
      // Finally set form values
      setOrderFormStates(orderFormValues);
    }
    setManualCashDialogOpen(false);
  };

  /**************** Ends Here *****************/

  /*****************************************
   *  Paying bill handler starts Here
   * *****************************************/

  /** @funcion handlePaymentDialogOpen */
  const handlePaymentDialogOpen = useCallback(
    (invoiceIds, printOnlys) => {
      dispatch(findOrder({ filter: { _id: invoiceIds } }));
      if (printOnlys) {
        setprintOnly(true);
      } else {
        setprintOnly(false);
      }
      setPaymentDialogOpen(true);
    },
    [setPaymentDialogOpen, dispatch]
  );

  /** @funcion handlePaymentDialogClose */
  const handlePaymentDialogClose = (event, _completePaymentData) => {
    if (_completePaymentData && _completePaymentData !== "backdropClick") {
      setCompletePaymentData(_completePaymentData);
      setPaymentDialogOpen(false);
      // Start printing
      if (!_completePaymentData.printOnly) {
        setTimeout(() => {
          window.print();
        }, 100);

        // After printing do the necessary reset
        window.onafterprint = () => {
          console.log("After print infomation");
          // If printOnly is true only take the printout
          let data = {
            filter: { _id: _completePaymentData?._id },
            update: {
              cashPaid: "Paid",
              paidBy: _completePaymentData.paidBy,
              total: _completePaymentData?.totalAmount,
            },
          };
          dispatch(updateOrder(data));
          setTimeout(() => {
            resetAll(); // Reset all fields
          }, 1000);
        };
      } else if (KOTprintOnly) {
        // If printOnly is true only take the printout
        setTimeout(() => {
          window.print();
        }, 100);

        // After printing do the necessary reset
        window.onafterprint = () => {
          let data = {
            filter: { _id: _completePaymentData?._id },
            update: _completePaymentData,
          };
          dispatch(updateOrder(data));
          setTimeout(() => {
            resetAll(); // Reset all fields
          }, 1000);
        };
      } else {
        if (window.confirm("Are you sure you want to complete payment?")) {
          // If printOnly is true only take the printout
          let data = {
            filter: { _id: _completePaymentData?._id },
            update: {
              cashPaid: "Paid",
              paidBy: _completePaymentData.paidBy,
              total: _completePaymentData?.totalAmount,
            },
          };
          dispatch(updateOrder(data));
        }
        setTimeout(() => {
          resetAll(); // Reset all fields
        }, 1000);
      }
    } else {
      setPaymentDialogOpen(false);
      setprintOnly(false);
      setKOTprintOnly(false);
    }
  };

  /***@function onHandleCancelOrder - function for handling the order cancellation */
  const onHandleCancelOrder = (_id) => {
    if (window.confirm("Are you sure you want to cancel this order?")) {
      let data = {
        filter: { _id: _id },
        update: {
          cashPaid: "Cancel",
          paidBy: "Cancel",
          total: 0,
        },
      };
      dispatch(updateOrder(data)); //Update order by sending cancel parameters
      setTimeout(() => {
        resetAll(); //Rest all the fields
      }, 1000);
    }
  };

  /******** Ends Here ******************/

  /** @function handleSaveOrders - on click of save function for the orders */
  const handleSaveOrders = () => {
    // If the order is already exist modify the  values otherwise create new one
    if (tableData) {
      tableData = { ...tableData, ...billingFormStates };
      tableData["orders"] = [...orderFormStates];
      tableData["total"] = totalPrice;
      let filter = {
        filter: { _id: tableData?._id },
        update: tableData,
      };
      dispatch(updateOrder(filter));
      setTimeout(() => {
        resetAll(); // Reset all fields
      }, 1000);
    } else {
      let newOrderPayload = billingFormStates;
      newOrderPayload.total = totalPrice;
      newOrderPayload["orders"] = orderFormStates;
      delete newOrderPayload["_id"];
      dispatch(onSaveOrder(newOrderPayload));
      setTimeout(() => {
        resetAll(); // Reset all fields
      }, 1000);
    }
  };

  const resetAll = () => {
    setOrderFormStates([]);
    dispatch(billingResetAll());
    setCompletePaymentData({}); // Set complete payment data to initial
    setPaymentInvoiceId("");
    // Need to replace somewhere or need to change the perfect logic
    loadInvoiceId();
    loadOrders();
    // end
    setBillingFormStates({
      invoiceId: invoiceId,
      tableNumber: " ",
    });
    setprintOnly(false);
    setKOTprintOnly(false);
  };

  /**@function onHandleKOTDialog */
  // const onHandleKOTDialog = (KOTOrders) => {
  //   setKOTprintOnly(true);
  //   handlePaymentDialogOpen(KOTOrders.invoiceId, false);
  // };

  return (
    <>
      {billingReducer?.loading ? (
        <Loading />
      ) : (
        <div className="container mt-3">
          <div className="ordersData">
            <h1 className="section-header">Billing</h1>
            <div className="row mt-3">
              <div className="col-md-8 ps-0 pe-0 pe-md-4">
                <div className="form-container">
                  <div className="row">
                    {invoiceInputs.map((element, index) => {
                      return (
                        <div className="col-md-4" key={index}>
                          <InputForm inputValues={element} />
                        </div>
                      );
                    })}
                  </div>
                </div>
                <div className="mt-3">
                  <div className="form-container addItem-container">
                    {billingFormStates.tableNumber &&
                    billingFormStates.tableNumber !== " " ? (
                      <>
                        <div className="row">
                          <div className="col-12 col-md-4 pe-0 ps-0">
                            {/* Auto complete goes here */}
                            <Autocomplete
                              {...defaultProps}
                              autoHighlight
                              onChange={(event, value) =>
                                handleOnSelectionOfItems(event, value)
                              }
                              clearOnBlur={true}
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Select Items"
                                  variant="standard"
                                  InputLabelProps={{ shrink: true }}
                                  autoFocus
                                />
                              )}
                            />
                            {/* Auto Complete ends */}
                          </div>
                          <div className="col-12 col-md-8 text-end pe-0 ps-0 mt-3 mt-md-0">
                            <button
                              type="button"
                              className="btn btn-light me-3 text-danger f-12"
                              onClick={() => handleManualCashDialogOpen(-1)}>
                              Manual Cash
                            </button>
                            <button
                              type="button"
                              className="btn btn-light me-3 text-success f-12"
                              onClick={() => handleSaveOrders()}
                              disabled={orderFormStates.length === 0}>
                              Save Order
                            </button>
                            {/* <button
                            type="button"
                            className="btn btn-light me-3 text-warning f-12"
                            onClick={() =>
                              handlePaymentDialogOpen(
                                tableData?.invoiceId,
                                false
                              )
                            }
                            disabled={orderFormStates.length === 0}
                          >
                            Pay Bill
                          </button> */}
                          </div>
                        </div>
                        {/* selected items list information */}
                        <div className="mt-3">
                          <table className="table table-responsive table-borderless table-hover order-table">
                            <thead className="bg-light text-secondary text-center">
                              <tr>
                                <th width="10%">SI.No</th>
                                <th width="50%">Item Name</th>
                                <th width="10%">Qty</th>
                                <th width="10%">Price</th>
                                <th width="10%">Edit</th>
                                <th width="10%">Delete</th>
                              </tr>
                            </thead>
                            {orderFormStates.length > 0 ? (
                              <>
                                <tbody>
                                  {orderFormStates.map((order, index) => (
                                    <tr key={index}>
                                      <td className="text-end">{index + 1}</td>
                                      <td>{order.itemName}</td>
                                      <td className="p-0 text-right">
                                        <TextField
                                          type="number"
                                          className="quantity"
                                          variant="standard"
                                          value={order.quantity}
                                          name="quantity"
                                          onChange={(e) =>
                                            onChangeQuantity(index, e)
                                          }
                                          onBlur={() => calculateKOT(index)}
                                          autoComplete="off"
                                          fullWidth
                                          required
                                        />
                                      </td>
                                      <td className="text-end">
                                        {order.price}
                                      </td>
                                      <td className="text-center">
                                        {order.cashAdded ? (
                                          <p
                                            className="table-buttons text-success m-0"
                                            onClick={() =>
                                              handleManualCashDialogOpen(index)
                                            }>
                                            Edit
                                          </p>
                                        ) : (
                                          ""
                                        )}
                                      </td>
                                      <td className="text-center">
                                        <p
                                          className="table-buttons text-danger m-0"
                                          onClick={() =>
                                            handleOnDeleteItems(index)
                                          }>
                                          Delete
                                        </p>
                                      </td>
                                    </tr>
                                  ))}
                                </tbody>
                                <tfoot>
                                  <tr className="text-end bg-light text-danger">
                                    <td colSpan={3}>
                                      <strong>Total</strong>
                                    </td>
                                    <td colSpan={1}>
                                      <strong>{totalPrice}</strong>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                  </tr>
                                </tfoot>
                              </>
                            ) : (
                              ""
                            )}
                          </table>
                        </div>
                        <ManualCashForm
                          open={openManualCashDialog}
                          onClose={handleOnDialogClose}
                          manualCashEdit={manualCashEditableData}
                        />
                      </>
                    ) : (
                      <div className="no-data text-center mt-5">
                        <p className="no-data-title">
                          Table number not selected
                        </p>
                        <p className="no-data-subtitle">
                          Please select table number to see the orders.
                        </p>
                      </div>
                    )}
                    {/* Selected items list ends */}
                  </div>
                </div>
              </div>
              <div className="col-md-4 mt-2 mt-md-0 p-0 p-md-auto">
                <h6 className="section-subHeading text-danger">All Orders</h6>
                <div className="mt-2">
                  {allOrders.length > 0 ? (
                    <>
                      <div className="row">
                        {allOrders.map((element, mainIndex) => (
                          <div key={mainIndex}>
                            <div className="col-md-12 ps-0 pe-0">
                              <div className="form-container mt-3">
                                <div className="row">
                                  <div className="col-md-6 ps-0">
                                    <p className="mb-2 table-number text-danger fw-bold">
                                      Table Number: {element.tableNumber}
                                    </p>
                                  </div>
                                  <div className="col-md-6 ps-0 text-end">
                                    <p className="mb-2 table-number text-danger fw-bold">
                                      Total:{" "}
                                      {element.total ? element.total : "0"}/-
                                    </p>
                                  </div>
                                  {/* <div className="col-md-6 ps-0 text-end fw-bold">
                                    <p
                                      className="table-buttons text-danger"
                                      onClick={() => onHandleKOTDialog(element)}
                                    >
                                      KOT
                                    </p>
                                  </div> */}
                                </div>
                                <div className="row">
                                  <div className="col-md-3 ps-0">
                                    <p
                                      className="table-buttons text-success fw-bold"
                                      onClick={() =>
                                        onSelectTableNumber(element.tableNumber)
                                      }>
                                      Edit
                                    </p>
                                  </div>
                                  <div className="col-md-3">
                                    <p
                                      className="table-buttons text-danger fw-bold"
                                      onClick={() =>
                                        onHandleCancelOrder(element._id)
                                      }>
                                      Cancel
                                    </p>
                                  </div>
                                  <div className="col-md-4">
                                    <p
                                      className="table-buttons text-info fw-bold"
                                      onClick={() =>
                                        handlePaymentDialogOpen(
                                          element._id,
                                          false
                                        )
                                      }>
                                      Print Bill
                                    </p>
                                  </div>
                                  {/* <div className="col-md-3">
                                    <p
                                      className="table-buttons text-danger text-end"
                                      onClick={() => onHandleKOTDialog(element)}
                                    >
                                      KOT
                                    </p>
                                  </div> */}
                                  {/* <div className="col-md-3 pe-0">
                                    <p
                                      className="table-buttons text-warning text-end"
                                      onClick={() =>
                                        handlePaymentDialogOpen(
                                          element.invoiceId,
                                          false
                                        )
                                      }
                                    >
                                      Pay Bill
                                    </p>
                                  </div> */}
                                </div>
                                <table className="table table-bordered table-hover">
                                  <thead className="bg-light text-secondary">
                                    <tr>
                                      <th>Item Name</th>
                                      <th>Quantity</th>
                                      <th>Price</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {allOrders[mainIndex].orders.map(
                                      (orderElement, orderIndex) => (
                                        <tr key={orderIndex}>
                                          <td>{orderElement.itemName}</td>
                                          <td>{orderElement.quantity}</td>
                                          <td>{orderElement.price}</td>
                                        </tr>
                                      )
                                    )}
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                      <PaymentDialog
                        open={openPaymentDialog}
                        onClose={handlePaymentDialogClose}
                        paymentData={paymentInvoiceId}
                        printOnly={printOnly}
                        KOTPrintOnly={KOTprintOnly}
                      />
                    </>
                  ) : (
                    <div className="no-data text-center mt-5">
                      <p className="no-data-title">Orders not created.</p>
                      <p className="no-data-subtitle">
                        Please create orders to visble orders here.
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          {Object.keys(completePaymentData).length > 0 ? (
            <>
              {/*--------------------------------- Print section ------------------- */}
              <div id="print-section">
                <div className="print-top-container">
                  <p className="printTitle">TRIPARVA RESTAURANT</p>
                  <p className="printSub-title">
                    Veg & Non-Veg Family Restaurant
                  </p>
                  <p className="print-address">
                    PB Road, Beside Ayurvedic College, Haveri
                  </p>
                  <p className="print-address">Mobile: +91 74837 40020</p>
                </div>
                <div className="memo-container">
                  <div className="container-information">
                    <div className="row">
                      <div className="col-2">
                        T.No:&nbsp;{completePaymentData.tableNumber}
                      </div>
                      <div className="col-10 text-end">
                        S.Name:&nbsp;{completePaymentData.supplierName}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="memo-container">
                  <div className="container-information">
                    <div className="row">
                      <div className="col-3">
                        {completePaymentData.invoiceId}
                      </div>
                      <div className="col-9 text-end">
                        {completePaymentData.date}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="itemsList-container">
                  <table className="table-order">
                    <thead>
                      <tr>
                        <th className="text-start">Particulars</th>
                        <th className="text-end">Qty</th>
                        <th className="text-end">Rate</th>
                        <th className="text-end">Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      {completePaymentData.orders.map((el, index) => {
                        return (
                          <tr key={index}>
                            <td>{el.itemName}</td>
                            <td className="text-end">{el.quantity}</td>
                            <td className="text-end">{el.singlePrice}</td>
                            <td className="text-end">&#x20B9;{el.price}</td>
                          </tr>
                        );
                      })}
                      <tr>
                        <th className="text-start">Total</th>
                        <th colSpan="3" className="text-end">
                          &#x20B9;{completePaymentData.totalAmount}
                        </th>
                      </tr>
                    </tbody>
                  </table>
                  <p className="thanks">ನಮ್ಮ ತ್ರಿಪರ್ವ</p>
                  <p className="thanks">ಧನ್ಯವಾದಗಳು. ಪುನಃ ಭೇಟಿ ನೀಡಿ.</p>
                  <p className="thanks-insta">
                    <svg
                      style={{ width: "14px", height: "14px" }}
                      viewBox="0 0 24 24">
                      <path
                        fill="currentColor"
                        d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z"
                      />
                    </svg>{" "}
                    namma_triparva
                  </p>
                </div>
              </div>
              {/* -------------------------------- Print section ends ---------------- */}
            </>
          ) : (
            ""
          )}
        </div>
      )}
    </>
  );
};
