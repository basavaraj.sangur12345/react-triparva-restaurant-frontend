import "./App.css";
import { PublicRoutes } from "./Routes/PublicRoutes";
import React from "react";

function App() {
  return (
    <div className="App">
      <PublicRoutes />
    </div>
  );
}

export default App;
